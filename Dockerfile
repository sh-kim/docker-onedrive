FROM ubuntu
MAINTAINER Sunghun Kim <shkim5@gmail.com>

RUN apt-get update
RUN apt-get -y install libcurl4-openssl-dev
RUN apt-get -y install gcc xdg-utils unzip make wget
RUN apt-get -y install libsqlite3-dev

RUN wget http://downloads.dlang.org/releases/2017/dmd_2.074.1-0_amd64.deb -O dmd.deb && dpkg -i dmd.deb

RUN wget https://github.com/skilion/onedrive/archive/master.zip && unzip master.zip

RUN cd onedrive-master && make && make install


VOLUME ["/root/OneDrive"]

RUN mkdir /root/.config

ADD ./onedrive.conf /root/.config/
ADD ./start.sh /root/

CMD /root/start.sh
