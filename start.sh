#!/bin/bash

if [ -f $HOME/onedrive.conf ]
then
  cp -f $HOME/.config/onedrive.conf $HOME/.config/onedrive_backup.conf
fi

if [ -f /usr/local/etc/onedrive.conf ]
then
  cp -f /usr/local/etc/onedrive.conf $HOME/.config/onedrive.conf
fi

/usr/local/bin/onedrive -m
