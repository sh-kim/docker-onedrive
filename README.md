# Docker-OneDrive

Based from https://bitbucket.org/jkoo/docker-onedrive ,

I built new docker image with updated versions of D compiler, OneDrive client ( https://github.com/skilion/onedrive )

## Quick Start

#### Step 1-A. Pull from Docker Hub

```shell
docker pull shkim5/docker-onedrive
```

#### Step 1-B. Build from local Dockerfile

```shell
docker build -t shkim5/docker-onedrive .
```

#### Step 2

```shell
mkdir OneDrive
docker run -it --restart on-failure --name docker-onedrive \
  -v `pwd`/OneDrive:/root/OneDrive \
  -v `pwd`/onedrive.conf:/usr/local/etc/onedrive.conf \
  shkim5/docker-onedrive
```
See https://bitbucket.org/jkoo/docker-onedrive for more detailed information.

